# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  upcase_str = []
  str.each_char {|el| upcase_str << el if el == el.upcase}
  upcase_str.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  answer = ""
  if str.length.odd?
    answer << str[str.length/2]
  else
    answer << str[(str.length/2)-1] << str[str.length/2]
  end
  answer
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
    str.count("aeiouAEIOU")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  answer = 1
  while num > 1
    answer *= num
    num -= 1
  end
  answer
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  answer = ""
  i = 0
  while i < arr.length
    if i != arr.length - 1
      answer << "#{arr[i]}#{separator}"
    else
      answer << "#{arr[i]}"
    end
    i += 1
  end
  answer
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  answer = ""
  str.split("").each_with_index do |word, idx|
    if idx.odd?
      answer << str[idx].capitalize
    else
      answer << str[idx].downcase
    end
  end
  answer
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  answer = []
  str.split(" ").each do |word|
    if word.length >= 5
      answer << word.reverse
    else
      answer << word
    end
  end
  answer.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  n_arr = []
  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      n_arr << "fizzbuzz"
    elsif num % 3 == 0
      n_arr << "fizz"
    elsif num % 5 == 0
      n_arr << "buzz"
    else
      n_arr << num
    end
  end
  n_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  rev_arr = []
  arr.each {|el| rev_arr.unshift(el)}
  rev_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  return true if num == 2
  (2..num/2).each do |n|
    return false if num % n == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors_arr = []
  (1..num).each {|el| factors_arr << el if num % el == 0}
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  answer = []
  factors(num).each {|el| answer << el if prime?(el)}
  answer
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = 0
  even = 0
  arr.each do |el|
    if el.odd?
      odd += 1
    else
      even += 1
    end
  end
  if odd > even
    arr.each {|el| return el if el.even?}
  else
    arr.each {|el| return el if el.odd?}
  end
end
